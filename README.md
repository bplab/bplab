# Workflow

## 1. Before Meeting

- Get ready for moving your card to next stage in next meeting.
    - **In Review**: Review all the issues within stage by clicking thumb-up bottom, ready for closing.
    - **Doing Doc**: All contents have been write onto the docs, ready for review.
    - **Doing**: All planned tasks are done, ready for documentation.
    - **ToDo**: All motivations and tasks are defined, such as 3W1H(Why, How, What, Where), ready for doing.
    - **Open**: make sure add the ideas into this stage before meeting start, ready for ToDo

## 2. Review Meeting/ Plan Meeting

- discuss any issues during last week.
- move all cards to next stage only if they are ready.

## 3. Retrospective Meeting

- discuss any suggestions/ideas which could improve the meeting.